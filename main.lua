
function love.load()
  -- Load the atlas.
  GAMEPAD_IMAGE = love.graphics.newImage('assets/image/gamepad-transparent.png')
  GAMEPAD_IMAGE:setFilter('nearest')
  KEYBOARD_IMAGE = love.graphics.newImage('assets/image/keyboard-transparent.png')
  KEYBOARD_IMAGE:setFilter('nearest')

  JOYSTICKS = love.joystick.getJoysticks()

  FONT_HEIGHT = 12 + 2
end

function love.update(dt)
  -- Quit game.
  if love.keyboard.isDown('escape', 'q') then
    love.event.push('quit')
  end

  -- :getGUID
  -- :isVibrationSupported
  -- :getAxisCount
  -- :getButtonCount

  -- :getGamepadAxis
  -- :isGamepadDown
end

-- function drawGamepad(gamepad)
--   if gamepad:isGamepad() then

--     gamepad:getGamepadAxis('leftx')
--     gamepad:getGamepadAxis('lefty')
--   end
-- end

function love.draw(dt)
  love.graphics.print(('%d joysticks found'):format(#JOYSTICKS), 0, 0)
  for i, js in ipairs(JOYSTICKS) do
    text = ('JS %d | ID: %s | GUID: %s | Axes: %d | Buttons: %d | Connected: %s | isGamepad: %s'):format(
      i, js:getID(), js:getGUID(), js:getAxisCount(), js:getButtonCount(), js:isConnected(), js:isGamepad())
    love.graphics.print(text, 0, i*FONT_HEIGHT)
    -- if js:isGamepad() then
    --   drawGamepad(js)
    -- end
  end
end

function love.keypressed(key, scancode, isrepeat)
end
